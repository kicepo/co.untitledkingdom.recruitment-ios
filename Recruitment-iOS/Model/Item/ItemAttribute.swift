//
//  ItemAttribute.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct ItemAttribute: Codable, ItemDetailProtocol{
    private enum CodingKeys: String, CodingKey {
        case name, color, preview, desc
    }
    
    var name: String
    var color: String
    let desc: String
    let preview: String
    
    init(name: String, color: String, desc: String, preview: String) {
        self.name = name
        self.color = color
        self.desc = desc
        self.preview = preview
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try values.decode(String.self, forKey: .name)
        self.color = try values.decode(String.self, forKey: .color)

        guard let description = try? values.decode(String.self, forKey: .desc) else {
            self.desc = ""
            self.preview = try values.decode(String.self, forKey: .preview)
            return
        }
        self.preview = ""
        self.desc = description
        
    }
}
