//
//  ItemData.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct ItemData<T:Codable>: Codable {
    private enum CodingKeys: String, CodingKey  {
        case data
    }
    
    var data: T
}
