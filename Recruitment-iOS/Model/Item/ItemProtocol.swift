//
//  ItemProtocol.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ItemProtocol {
    associatedtype attribute
    var id: String { get }
    var type: String { get }
    var attributes: attribute { get }
}

protocol ItemDetailProtocol {
    var name: String { get }
    var color: String { get }
}
