//
//  Item.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct Item<T: Codable>: Codable, ItemProtocol {
    typealias attribute = T
    
    var id: String
    var type: String
    var attributes: T
    
    private enum CodingKeys: String, CodingKey {
        case id, type, attributes
    }
}
