//
//  ItemTableView.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

private let reusableIdentifier = "TableViewIdentifier"

class ItemTableView: UITableView {
    
    weak var selectionDelegate: ItemSelectionDelegate?
    
    private var items: [ItemViewModel<ItemAttribute>] {
        didSet {
                self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        self.items = []
        super.init(frame: frame, style: style)
        self.dataSource = self
        self.delegate = self
        self.register(ItemTableViewCell.self, forCellReuseIdentifier: reusableIdentifier)
        self.estimatedRowHeight = 100
        self.alwaysBounceVertical = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ItemTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectionDelegate?.handler(to: items[indexPath.row])
    }
}

extension ItemTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath) as? ItemTableViewCell
        cell?.item = items[indexPath.row]
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ItemTableView: CollectionProtocol {
    func setNew(items: [ItemViewModel<ItemAttribute>]) {
        self.items = items
    }
}
