//
//  ItemTableViewCell.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    var item: ItemViewModel<ItemAttribute>? {
        didSet {
            self.configure()
        }
    }
    
    public var titleLabel: FitLabel = {
        return FitLabel()
    }()
    
    private var previewLabel: FitLabel = {
       return FitLabel()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.setupConstaints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func setupConstaints() {
        let stack = UIStackView(arrangedSubviews: [titleLabel, previewLabel])
        stack.axis = .vertical
        stack.spacing = 4
        
        self.addSubview(stack)
        stack.anchor(top: self.safeAreaLayoutGuide.topAnchor,
                     left: self.safeAreaLayoutGuide.leftAnchor,
                     bottom: self.safeAreaLayoutGuide.bottomAnchor,
                     right: self.safeAreaLayoutGuide.rightAnchor,
                     paddingTop: 4, paddingLeft: 4, paddingBottom: 4, paddingRight: 4)
    }
}

extension ItemTableViewCell: ItemCellProtocol {
    internal func configure() {
        guard let item = item else { return }
        self.titleLabel.text = item.name
        self.previewLabel.text = item.preview
        self.backgroundColor = item.color
    }
}
