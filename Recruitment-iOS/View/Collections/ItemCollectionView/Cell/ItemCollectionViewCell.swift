//
//  ItemCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    var item: ItemViewModel<ItemAttribute>? {
        didSet {
            self.configure()
        }
    }
    
    public var nameLabel: UILabel = {
        return UILabel()
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.setupConstaints()
    }
    
    internal func setupConstaints() {
        self.addSubview(nameLabel)
        nameLabel.centerX(inView: self)
        nameLabel.centerY(inView: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ItemCollectionViewCell: ItemCellProtocol {
    internal func configure() {
        guard let item = item else { return }
        self.nameLabel.text = item.name
        self.backgroundColor = item.color
    }
}
