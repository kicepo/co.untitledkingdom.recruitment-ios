//
//  ItemCollectionView.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

private let reusableIdentifier = "CollectionIdentifier"

class ItemCollectionView: UICollectionView {
    
    weak var selectionDelegate: ItemSelectionDelegate?
    
    private var items: [ItemViewModel<ItemAttribute>] {
        didSet {
                self.reloadData()
        }
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        self.items = []
        super.init(frame: frame, collectionViewLayout: layout)
        self.backgroundColor = .white
        
        self.dataSource = self
        self.delegate = self
        self.register(ItemCollectionViewCell.self, forCellWithReuseIdentifier: reusableIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ItemCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectionDelegate?.handler(to: items[indexPath.row])
    }
    
}

extension ItemCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusableIdentifier, for: indexPath) as? ItemCollectionViewCell
        cell?.item = items[indexPath.row]
        return cell ?? UICollectionViewCell()
    }
}

extension ItemCollectionView: CollectionProtocol {
    func setNew(items: [ItemViewModel<ItemAttribute>]) {
        self.items = items
    }
}
