//
//  ItemCellProtocol.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ItemCellProtocol: class {
    var item: ItemViewModel<ItemAttribute>? { get set }
    func configure()
    func setupConstaints()
}
