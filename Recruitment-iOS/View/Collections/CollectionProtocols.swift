//
//  CollectionProtocols.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol CollectionProtocol: class {
    var selectionDelegate: ItemSelectionDelegate? { get set }
    func setNew(items: [ItemViewModel<ItemAttribute>])
}

protocol ItemSelectionDelegate: class {
    func handler(to item: ItemViewModel<ItemAttribute>)
}
