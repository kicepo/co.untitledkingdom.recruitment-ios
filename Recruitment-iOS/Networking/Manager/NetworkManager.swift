//
//  NetworkManager.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct NetworkManager {
    public static func getItems<T: Codable>(itemName: String, completion: @escaping (Result<T, NetworkError>) -> Void) {
        NetworkRequestManager.request(for: itemName) { (result) in
            switch result {
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(ItemData<T>.self, from: data)
                    completion(.success(result.data))
                } catch {
                    completion(.failure(.decode(error.localizedDescription)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
