//
//  NetworkRequestManager.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

struct NetworkRequestManager {
    static func request(for fileName: String, completion: @escaping (Result<Data, NetworkError>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path))
                    completion(.success(data))
                } catch {
                    completion(.failure(.parse(error.localizedDescription)))
                }
            } else {
                completion(.failure(.invalidPath))
            }
        }
    }
}
