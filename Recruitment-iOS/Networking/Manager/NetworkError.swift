//
//  NetworkError.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

enum NetworkError: Error, Equatable {
    case unknown
    case loadData
    case invalidPath
    case decode(String)
    case parse(String)
}
