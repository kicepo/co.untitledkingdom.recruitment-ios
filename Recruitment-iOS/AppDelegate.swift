//
//  AppDelegate.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func applicationDidFinishLaunching(_ application: UIApplication) {
        guard let window = window else {
            return
        }
        let initialVC = UINavigationController(rootViewController: StartViewController())
        window.backgroundColor = .darkGray
        window.rootViewController = initialVC
        window.makeKeyAndVisible()
    }

}

