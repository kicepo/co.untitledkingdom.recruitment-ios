//
//  StartViewController.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    private let startButton: UIButton = {
        let button = UIButton()
        button.setTitle("Start", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(handleClick), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(startButton)
        self.startButton.centerX(inView: self.view)
        self.startButton.centerY(inView: self.view)
    }
    
    // MARK: === Selectors
    @objc func handleClick() {
        let tbVC = TableViewController()
        tbVC.tabBarItem = UITabBarItem(title: "TableView",
                                       image: UIImage(systemName: "list.bullet"),
                                       selectedImage: UIImage(named: "list.bullet"))
        
        let clVC = CollectionViewController()
        clVC.tabBarItem = UITabBarItem(title: "CollectionView",
                                       image: UIImage(systemName: "rectangle.on.rectangle"),
                                       selectedImage: UIImage(named: "rectangle.on.rectangle"))
        
        let tabBar = UITabBarController()
        tabBar.viewControllers = [tbVC, clVC]
        
        navigationController?.pushViewController(tabBar, animated: true)
    }
}
