//
//  BaseTBCViewController.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class BaseTBCViewController<T: CollectionProtocol>: UIViewController {
    
    var collectionView: T?
    
    private var spinner: UIView?
    
    func setupConstaints() {
        guard let collectionUnwraped = self.collectionView else {
            assertionFailure("Collection must exist")
            return
        }
        
        guard let collection = collectionUnwraped as? UIView else {
            assertionFailure("Collection must be UIView")
            return
        }
        
        self.view.addSubview(collection)
        
        collection.anchor(top: self.view.safeAreaLayoutGuide.topAnchor,
                          left: self.view.safeAreaLayoutGuide.leftAnchor,
                          bottom: self.view.safeAreaLayoutGuide.bottomAnchor,
                          right: self.view.safeAreaLayoutGuide.rightAnchor)
        
    }
    
    func setupNetworkManager() {
        self.showSpinner()
        NetworkManager.getItems(itemName: "Items") { [weak self] (_ result: Result<[Item<ItemAttribute>], NetworkError>) in
            switch result {
            case .success(let items):
                let itemsVM = items.map {
                    return ItemViewModel(item: $0)
                }
                self?.collectionView?.setNew(items: itemsVM)
            case .failure(let error):
                self?.showAlert(error: error)
            }
            self?.removeSpinner()
        }
    }
    
    private func showAlert(error: Error) {
        let alert = UIAlertController(title: "Error", message: "Error - \(error)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        self.present(alert, animated: true)
    }
}

extension BaseTBCViewController: ItemSelectionDelegate {
    func handler(to item: ItemViewModel<ItemAttribute>) {
        let vc = DetailsViewController()
        vc.item = item
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension BaseTBCViewController {
    private func showSpinner() {
        self.spinner = UIView.init(frame: self.view.frame)
        self.spinner?.backgroundColor = UIColor.init(white: 1.0, alpha: 0.3)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.startAnimating()
        ai.center = self.spinner!.center
        
        DispatchQueue.main.async { [weak self] in
            guard let spinner = self?.spinner else {
                return
            }
            spinner.addSubview(ai)
            self?.view.addSubview(spinner)
        }
    }
    
    private func removeSpinner() {
        DispatchQueue.main.async { [weak self] in
            self?.spinner?.removeFromSuperview()
        }
    }
}
