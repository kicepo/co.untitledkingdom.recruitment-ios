//
//  TableViewController.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class TableViewController: BaseTBCViewController<ItemTableView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.collectionView = ItemTableView(frame: .zero, style: .plain)
        self.collectionView!.selectionDelegate = self
        
        self.setupConstaints()
        self.setupNetworkManager()
    }
}
