//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewController: BaseTBCViewController<ItemCollectionView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.setupCollection()
        self.setupConstaints()
        self.setupNetworkManager()
    }
    
    private func setupCollection() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        layout.itemSize = CGSize(width: self.view.frame.width / 2 - 16, height: self.view.frame.height / 4)
        
        self.collectionView = ItemCollectionView(frame: .zero, collectionViewLayout: layout)
        self.collectionView?.selectionDelegate = self
    }
}
