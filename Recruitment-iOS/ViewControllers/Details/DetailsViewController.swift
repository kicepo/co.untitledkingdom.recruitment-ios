//
//  DetailsViewController.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    public var item: ItemViewModel<ItemAttribute>? = nil
    
    private let textView: UITextView = {
        let textView = UITextView()
        textView.text = "Loading..."
        textView.textAlignment = .left
        textView.backgroundColor = .clear
        textView.font = UIFont.init(name: "Arial", size: 14)
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let item = item else { return }
        self.view.backgroundColor = item.color
        
        setupConstaints()
        networkRequest()
    }
    
    private func setupConstaints() {
        self.view.addSubview(textView)
        self.textView.anchor(top: self.view.safeAreaLayoutGuide.topAnchor,
                              left: self.view.safeAreaLayoutGuide.leftAnchor,
                              bottom: self.view.safeAreaLayoutGuide.bottomAnchor,
                              right: self.view.safeAreaLayoutGuide.rightAnchor)
    }
    
    private func networkRequest() {
        guard let itemName = item?.name else { return }
        NetworkManager.getItems(itemName: itemName) { [weak self] (_ result: Result<Item<ItemAttribute>, NetworkError>) in
            switch result {
            case .success(let item):
                self?.textView.text = item.attributes.desc
                self?.setupTitle(title: item.attributes.name)
            case .failure(let error):
                self?.textView.text = "Error: \(error)"
                self?.setupTitle(title: "Error")
            }
        }
    }
    
    private func setupTitle(title: String) {
        self.title = makeTitle(from: title)
    }
    
    func makeTitle(from: String) -> String {
        let title = from.enumerated().map({
                        $0.offset % 2 == 0 ? String($0.element).uppercased() : String($0.element).lowercased()
                    }).joined()
        return title
    }
}
