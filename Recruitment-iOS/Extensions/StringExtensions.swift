//
//  StringExtensions.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

import UIKit

extension String {
    var getColor: UIColor {
        switch self.lowercased() {
        case "blue":
            return .blue
        case "yellow":
            return .yellow
        case "red":
            return .red
        case "green":
            return .green
        case "purple":
            return .purple
        default:
            return .black
        }
    }
    
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func lowercasingFirstLetter() -> String {
        return prefix(1).lowercased() + dropFirst()
    }
    
    mutating func lowercasedFirstLetter() {
        self = self.lowercasingFirstLetter()
    }
}
