//
//  ItemViewModel.swift
//  Recruitment-iOS
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

struct ItemViewModel<T: Codable> {
    
    private let item: Item<T>
    private var itemAttribute: ItemAttribute?
    
    var id: String {
        return item.id
    }
    
    var type: ItemType {
        return ItemType(rawValue: item.type.lowercasingFirstLetter()) ?? .undefined
    }
    
    var name: String {
        return itemAttribute?.name.capitalizingFirstLetter() ?? ""
    }
    
    var description: String {
        return itemAttribute?.desc.capitalizingFirstLetter() ?? ""
    }
    
    var preview: String {
        return itemAttribute?.preview.capitalizingFirstLetter() ?? ""
    }
    
    var color: UIColor {
        return itemAttribute?.color.getColor ?? .black
    }
    
    init(item: Item<T>) {
        self.item = item
        self.itemAttribute = nil
        
        if let attribute = item.attributes as? ItemAttribute {
            self.itemAttribute = attribute
        }
    }
    
}
