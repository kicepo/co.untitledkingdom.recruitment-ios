//
//  TCViewControllersTests.swift
//  Recruitment-iOSTests
//
//  Created by Kacper Szczepankiewicz on 01/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class TCViewControllersTests: XCTestCase {
    
    func testIsLoadedCollection() throws {
        let vc = CollectionViewController()
        
        vc.loadView()
        vc.viewDidLoad()
        
        XCTAssertNotNil(vc.collectionView)
    }
    
    func testViewIsWhiteBGAtCVC() throws {
        let vc = CollectionViewController()
        
        vc.loadView()
        vc.viewDidLoad()
        
        XCTAssertEqual(vc.view.backgroundColor, .white)
    }
    
    func testIsLoadedTableView() throws {
        let vc = TableViewController()
        
        vc.loadView()
        vc.viewDidLoad()
        
        XCTAssertNotNil(vc.collectionView)
    }
    
    func testViewIsWhiteBGAtTVC() throws {
        let vc = TableViewController()
        
        vc.loadView()
        vc.viewDidLoad()
        
        XCTAssertEqual(vc.view.backgroundColor, .white)
    }
    
}
