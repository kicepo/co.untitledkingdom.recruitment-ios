//
//  ItemViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ItemViewModelTests: XCTestCase {
    
    var itemVm: ItemViewModel<ItemAttribute>?
    
    override func setUpWithError() throws {
        let itemAttribute: ItemAttribute = ItemAttribute(name: "test", color: "green", desc: "no description", preview: "no preview")
        let item = Item(id: "1", type: "itemDetails", attributes: itemAttribute)
        self.itemVm = ItemViewModel(item: item)
    }

    override func tearDownWithError() throws {
        self.itemVm = nil
    }
    
    func testViewModelItemAttribute() {
        let vm = self.itemVm!
        
        XCTAssertEqual(vm.color, .green)
        XCTAssertEqual(vm.description, "No description")
        XCTAssertEqual(vm.preview, "No preview")
        XCTAssertNotEqual(vm.description, "No Description")
        XCTAssertNotEqual(vm.preview, "No Preview")
    }
    
    func testViewModelItem() {
        let vm = self.itemVm!
        
        XCTAssertEqual(vm.id, "1")
        XCTAssertEqual(vm.type, ItemType.itemDetails)
    }
    
}

