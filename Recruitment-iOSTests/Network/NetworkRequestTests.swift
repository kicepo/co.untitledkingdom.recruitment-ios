//
//  NetworkRequestTests.swift
//  Recruitment-iOSTests
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class NetworkRequestTest: XCTestCase {
    
    var fileData: Data?
    let file = "Item1"
    
    override func setUpWithError() throws {
        self.loadData()
    }

    override func tearDownWithError() throws {
        self.fileData = nil
    }

    private func loadData() {
        if let path = Bundle.main.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                self.fileData = data
            } catch {
                print(error)
            }
            
        }
    }
    
    func testNetworkRequestFailByPath() throws {
        let testError: NetworkError = .invalidPath
        
        let expectation = XCTestExpectation(description: "Waiting for response from NetworkRequestManager")
        
        NetworkRequestManager.request(for: "") { (result) in
            switch result {
            case .success(_): break
            case .failure(let error):
                XCTAssertEqual(error, testError)
            
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
        
    }
    
    func testNetworkRequestSuccess() throws {
       
        let expectation = XCTestExpectation(description: "Waiting for response with success case")
        
        NetworkRequestManager.request(for: "Item1") { (result) in
            switch result {
            case .success(let data):
                XCTAssertEqual(data, self.fileData)
            case .failure(_):
                break
            }
            
            expectation.fulfill()
            
        }
        
        wait(for: [expectation], timeout: 5)
        
    }

}
