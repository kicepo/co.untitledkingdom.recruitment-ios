//
//  DetailsViewControllerTests.swift
//  Recruitment-iOSTests
//
//  Created by Kacper Szczepankiewicz on 02/03/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class DetailsViewControllerTests: XCTestCase {
    
    var vc: DetailsViewController?
    
    override func setUpWithError() throws {
        self.vc = DetailsViewController()
        vc?.loadView()
        vc?.viewDidLoad()
    }

    override func tearDownWithError() throws {
        self.vc = nil
    }
    
    
    func testIsTitleValid() throws {
        let expectedString = "TeStStRiNg"
        let expectedString_2 = "ItEm0210PeR"
        
        XCTAssertEqual(vc?.makeTitle(from: "testString"), expectedString)
        XCTAssertEqual(vc?.makeTitle(from: "ITEM0210PER"), expectedString_2)
    }
    
    func testIsInvalidTItleValidation() {
        let notExpectedString = "tEsTsTrInG"
        let notExpectedString_2 = "ITEm0210pEr"
        let notExpectedString_3 = "SooooLongString"
        
        XCTAssertNotEqual(vc?.makeTitle(from: "testString"), notExpectedString)
        XCTAssertNotEqual(vc?.makeTitle(from: "ITEM0210PER"), notExpectedString_2)
        XCTAssertNotEqual(vc?.makeTitle(from: "sooooLongString"), notExpectedString_3)
        
    }
}

